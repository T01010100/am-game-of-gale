package ch.matlaw.gameofgale;

import ch.matlaw.gameofgale.player.HumanPlayer;
import ch.matlaw.gameofgale.player.Player;
import ch.matlaw.gameofgale.player.RandomPlayer;
import ch.matlaw.modifyarray.ModifyArray;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Line;


/**
 * @author moritzbolliger
 * @package ch.mbcoding.main
 * @project AMBridgeItGame
 * @date 2020-03-17
 **/
public class GuiController implements Notifier {

    @FXML
    private Button btnReStart;

    @FXML
    private GridPane grid;

    @FXML
    private Line lb11, lb13, lb57, lb75, lb55, lb53, lb73, lb71, lb51, lb37, lb35, lb33, lb31, lb17, lb15,
            lb77, lb22, lb66, lb46, lb26, lb64, lb44, lb24, lb62, lb42;
    @FXML
    private Line lr11, lr77, lr57, lr37, lr17, lr75, lr55, lr35, lr15, lr73, lr53, lr33, lr13, lr71,
            lr51, lr31, lr22, lr66, lr46, lr26, lr64, lr44, lr24, lr62, lr42;

    @FXML
    private Pane p11, p31, p51, p71, p22, p42, p62, p13, p33, p53, p73, p24, p44, p64, p15, p35, p55, p75, p26, p46, p66, p17, p37, p57, p77;

    @FXML
    private Label labelInfo;

    private static GuiController instance;
    private Field[] fields;

    public GuiController() {
        fields = new Field[0];
        instance = this;
    }

    public void test(){
        clickable(p11, lb11, lr11, 1);
        clickable(p31, lb31, lr31, 5);
        clickable(p51, lb51, lr51, 9);
        clickable(p71, lb71, lr71, 13);
        clickable(p22, lb22, lr22, 17);
        clickable(p42, lb42, lr42, 20);
        clickable(p62, lb62, lr62, 23);
        clickable(p13, lb13, lr13, 2);
        clickable(p33, lb33, lr33, 6);
        clickable(p53, lb53, lr53, 10);
        clickable(p73, lb73, lr73, 14);
        clickable(p24, lb24, lr24, 18);
        clickable(p44, lb44, lr44, 21);
        clickable(p64, lb64, lr64, 24);
        clickable(p15, lb15, lr15, 3);
        clickable(p35, lb35, lr35, 7);
        clickable(p55, lb55, lr55, 11);
        clickable(p75, lb75, lr75, 15);
        clickable(p26, lb26, lr26, 19);
        clickable(p46, lb46, lr46, 22);
        clickable(p66, lb66, lr66, 25);
        clickable(p17, lb17, lr17, 4);
        clickable(p37, lb37, lr37, 8);
        clickable(p57, lb57, lr57, 12);
        clickable(p77, lb77, lr77, 16);

        btnReStart.setOnAction(event -> {
            labelInfo.setText("");

            lb11.setVisible(false);
            lb31.setVisible(false);
            lb51.setVisible(false);
            lb71.setVisible(false);
            lb22.setVisible(false);
            lb42.setVisible(false);
            lb62.setVisible(false);
            lb13.setVisible(false);
            lb33.setVisible(false);
            lb53.setVisible(false);
            lb73.setVisible(false);
            lb24.setVisible(false);
            lb44.setVisible(false);
            lb64.setVisible(false);
            lb15.setVisible(false);
            lb35.setVisible(false);
            lb55.setVisible(false);
            lb75.setVisible(false);
            lb26.setVisible(false);
            lb46.setVisible(false);
            lb66.setVisible(false);
            lb17.setVisible(false);
            lb37.setVisible(false);
            lb57.setVisible(false);
            lb77.setVisible(false);

            lr11.setVisible(false);
            lr31.setVisible(false);
            lr51.setVisible(false);
            lr71.setVisible(false);
            lr22.setVisible(false);
            lr42.setVisible(false);
            lr62.setVisible(false);
            lr13.setVisible(false);
            lr33.setVisible(false);
            lr53.setVisible(false);
            lr73.setVisible(false);
            lr24.setVisible(false);
            lr44.setVisible(false);
            lr64.setVisible(false);
            lr15.setVisible(false);
            lr35.setVisible(false);
            lr55.setVisible(false);
            lr75.setVisible(false);
            lr26.setVisible(false);
            lr46.setVisible(false);
            lr66.setVisible(false);
            lr17.setVisible(false);
            lr37.setVisible(false);
            lr57.setVisible(false);
            lr77.setVisible(false);

            Game.getInstance().newGame(7);

            Game.getInstance().setUpBluePlayer(new HumanPlayer());
            Game.getInstance().setUpRedPlayer(new HumanPlayer());

            Game.getInstance().start();
        });

        Game.getInstance().newGame(7);

        Game.getInstance().setUpBluePlayer(new HumanPlayer());
        Game.getInstance().setUpRedPlayer(new HumanPlayer());

        Game.getInstance().setWinNotifier(this);

        Game.getInstance().start();
    }

    private void clickable(Pane pane, Line blue, Line red, int position) {

        fields = new ModifyArray.ADD<>(Field.class, fields, new Field(position, blue, red)).getArray();

        pane.setOnMouseClicked(event -> {
            if (Game.getInstance().getWon() != 0) return;

            Player playerOnTurn = Game.getInstance().getPlayerOnTurn();

            if (!(playerOnTurn instanceof HumanPlayer))
                return;

            Board currentBoard = Game.getInstance().getCurrentBoard();
            int playerValue = playerOnTurn.getValue();

            if (currentBoard.getValue(position-1) == 0) {
                currentBoard.setValue(position-1, playerValue);

                Field field = getField(position);
                field.mark(playerValue);
            }

            ((HumanPlayer) playerOnTurn).setTurnOver();
        });
    }

    public Field getField(int position) {
        for (Field field : fields) {
            if (field.position == position)
                return field;
        }
        return null;
    }

    public static GuiController getInstance() {
        return instance;
    }

    @Override
    public void notifyMe() {
        String color = Game.getInstance().getWon() == 1 ? "Red" : "Blue";

        Platform.runLater(() -> labelInfo.setText(color + " won!"));
    }

    public static class Field {
        private int position;
        private Line blue;
        private Line red;

        public Field(int position, Line blue, Line red) {
            this.position = position;
            this.blue = blue;
            this.red = red;
        }

        public void mark(int value) {
            if (value == 1)
                red.setVisible(true);
            else
                blue.setVisible(true);
        }
    }
}
