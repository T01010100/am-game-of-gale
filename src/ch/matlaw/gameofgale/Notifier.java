package ch.matlaw.gameofgale;

public interface Notifier {
    void notifyMe();
}
