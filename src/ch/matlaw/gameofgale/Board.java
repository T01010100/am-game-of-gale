package ch.matlaw.gameofgale;

public class Board {
    private int width;
    private int[] board;
    private boolean invalid;

    private int sectionOffset;

    private Board() {
        this.invalid = true;
        board = new int[0];
    }

    private Board(int width) {
        this.width = width;
        this.invalid = false;
    }

    public void print() {
        for (int i : board) {
            System.out.print(i + " ");
        }
        System.out.println();
    }

    public static Board createBoard(int width) {
        if (width < 3) {
            System.out.println("width must be at least 3");
            return new Board();
        }
        if (width % 2 == 0) {
            System.out.println("board width must be odd");
            return new Board();
        }

        Board board = new Board(width);

        int amount = board.calculateAmount();
        board.board = new int[amount];
        board.sectionOffset = (int) Math.pow(board.calculateRows(), 2);

        return board;
    }

    public static Board clone(Board original) {
        if (original.invalid) {
            System.out.println("cloned board is invalid");
            return original;
        }

        Board clone = createBoard(original.width);

        int amount = original.calculateAmount();
        if (amount >= 0)
            System.arraycopy(original.board, 0, clone.board, 0, amount);

        return clone;
    }

    public int calculateRows() {
        return (width - 1) / 2 + 1;
    }

    public int calculateAmount() {
        int rows = calculateRows();
        return (int) (Math.pow(rows, 2) + Math.pow(rows - 1, 2));
    }

    public int[] getRows() {
        int amount = this.sectionOffset;
        int[] rowContents = new int[amount];
        System.arraycopy(this.board, 0, rowContents, 0, amount);
        return rowContents;
    }

    public int[] getConnections() {
        int amount = this.board.length - this.sectionOffset;
        int[] rowContents = new int[amount];
        System.arraycopy(this.board, this.sectionOffset, rowContents, 0, amount);
        return rowContents;
    }

    public int getValue(int index) {
        if (this.invalid)
            return 0;
        return this.board[index];
    }

    public void setValue(int index, int value) {
        if (!this.invalid)
            this.board[index] = value;
    }

    public int getWidth() {
        return width;
    }

    public boolean isInvalid() {
        return invalid;
    }

    public int[] getBoard() {
        return this.board;
    }

    public int getSectionOffset() {
        return sectionOffset;
    }
}
