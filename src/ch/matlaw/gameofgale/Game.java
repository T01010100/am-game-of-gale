package ch.matlaw.gameofgale;

import ch.matlaw.gameofgale.player.Player;

public class Game extends Thread {
    private static Game game;

    private Board currentBoard;
    private Player redPlayer;
    private Player bluePlayer;

    private int turn;

    private int won;

    private Notifier winNotifier;

    private Game() {}

    public static Game getInstance() {
        if (game == null)
            game = new Game();
        return game;
    }

    @Override
    public void run() {
        super.run();
        startGame();
    }

    public void newGame(int width) {
        Game newGame = new Game();
        newGame.currentBoard = Board.createBoard(width);

        newGame.setWinNotifier(winNotifier);

        game = newGame;
    }

    private void startGame() {
        won = 0;
        turn = 1;
        Player player;

        int won;

        do {
            if (turn == 1)
                player = redPlayer;
            else
                player = bluePlayer;

            player.notifyTurn();

            while (!player.isTurnOver()) {
                try {
                    Thread.sleep(50);
                } catch (InterruptedException ignored) {}
            }

            player.resetTurnOver();

            currentBoard.print();

            won = WinChecker.checkForWin(currentBoard, turn);

            if (turn == 1)
                turn = -1;
            else
                turn = 1;
        } while (won == 0);

        this.won = won;

        if (winNotifier != null)
            winNotifier.notifyMe();
    }

    public Player getPlayerOnTurn() {
        if (turn == 1)
            return redPlayer;
        else
            return bluePlayer;
    }

    public void setUpRedPlayer(Player player) {
        player.setValue(1);
        redPlayer = player;
        redPlayer.start();
    }

    public void setUpBluePlayer(Player player) {
        player.setValue(-1);
        bluePlayer = player;
        bluePlayer.start();
    }

    public Board getCurrentBoard() {
        return currentBoard;
    }

    public int getWon() {
        return won;
    }

    public void setWinNotifier(Notifier winNotifier) {
        this.winNotifier = winNotifier;
    }
}
