package ch.matlaw.gameofgale;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

/**
 * @author moritzbolliger
 * @package ch.mbcoding.main
 * @project AMBridgeItGame
 * @date 2020-03-17
 **/
public class App extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        //View
        FXMLLoader myLoader = new FXMLLoader(getClass().getResource("GUI.fxml"));
        BorderPane root = myLoader.load();

        //Control
        GuiController controller = myLoader.getController();


        primaryStage.setTitle("Game of Gale By David Gale");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
        controller.test();
    }
}
