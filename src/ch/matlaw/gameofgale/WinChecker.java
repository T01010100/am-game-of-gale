package ch.matlaw.gameofgale;

import ch.matlaw.modifyarray.ModifyArray;

public class WinChecker {
    private WinChecker() {}

    public static int checkForWin(Board currentBoard) {
        if (currentBoard.isInvalid())
            return 0;

        if (checkColor(currentBoard, 1))
            return 1;

        if (checkColor(currentBoard, -1))
            return -1;

        return 0;
    }

    public static int checkForWin(Board currentBoard, int turn) {
        if (currentBoard.isInvalid())
            return 0;

        if (checkColor(currentBoard, turn))
            return turn;

        return 0;
    }

    private static boolean checkColor(Board board, int value) {
        TokenCollection collection = new TokenCollection(board, value);
        if (value == -1) {
            for (int i = 0; i < board.calculateRows(); i++) {
                collection.addToken(i * board.calculateRows() + 1);
            }
        } else {
            for (int i = 0; i < board.calculateRows(); i++) {
                collection.addToken(i + 1);
            }
        }

        do {
            try {
                Thread.sleep(10);
            } catch (InterruptedException ignored) {}
        } while (!collection.isFinished());

        return collection.isWon();
    }

    private static class TokenCollection {
        private Board board;
        private int value;
        private Integer[] blacklist;
        private int started = 0;
        private int ended = 0;
        private boolean won = false;

        private TokenCollection(Board board, int value) {
            this.board = board;
            this.value = value;
            blacklist = new Integer[0];
        }

        private void addToBlacklist(int position) {
            this.blacklist = (Integer[]) new ModifyArray.ADD(Integer.class, blacklist, position).getArray();
        }

        private boolean isOnBlacklist(int position) {
            for (int i : blacklist) {
                if (i == position)
                    return true;
            }
            return false;
        }

        private void addToken(int position) {
            started ++;
            Token token = new Token(position);
            token.parent = this;
            token.start();
        }

        public boolean isFinished() {
            return started == ended;
        }

        public boolean isWon() {
            return won;
        }
    }

    public static void main(String[] args) {
        Token token = new Token(1);
        token.parent = new TokenCollection(Board.createBoard(7), 1);
        token.activateNeighbors();
    }

    private static class Token extends Thread {
        private TokenCollection parent;
        private int position;

        private Token(int position) {
            this.position = position;
        }

        @Override
        public void run() {
            move();
            parent.ended++;
        }

        private void move() {
            if (parent.isOnBlacklist(position))
                return;

            if (position < 1 || position > parent.board.calculateAmount())
                return;

            if (parent.board.getValue(position - 1) != parent.value)
                return;

            if ((position <= parent.board.getSectionOffset() && position % parent.board.calculateRows() == 0) && parent.value == -1)
                parent.won = true;

            if (position > parent.board.calculateRows() * (parent.board.calculateRows() - 1) && position <= parent.board.getSectionOffset() && parent.value == 1)
                parent.won = true;

            parent.addToBlacklist(position);
            activateNeighbors();
        }

        private void activateNeighbors() {
            int w = parent.board.calculateRows();
            int f = (int) Math.pow(w, 2);
            if (parent.board.getSectionOffset() < position) { // connection row
                int x = (position - f);
                int hp = x / (w - 1);

                boolean top = (position - f) % (w - 1) != 1;
                boolean bottom = (position - f) % (w - 1) != 0;
                boolean right = (position - f) / (w - 1) < w - 2;
                boolean left = (position - f) > (w - 1);

                parent.addToken(x + hp);
                parent.addToken(x + hp + w);
                parent.addToken(x + hp + 1);
                parent.addToken(x + hp + w + 1);

                if (parent.value == -1) {
                    if (right)
                        parent.addToken(position + (w - 1));
                    if (left)
                        parent.addToken(position - (w - 1));
                } else {
                    if (top)
                        parent.addToken(position - 1);
                    if (bottom)
                        parent.addToken(position + 1);
                }
            } else { // big row
                int vp = (position-1) % w;
                int hp = position / w;

                boolean top = position % w != 1;
                boolean bottom = position % w != 0;
                boolean right = position / w < w - 1;
                boolean left = position > w;

                if (top && left)
                    parent.addToken(f + vp + ((hp - 1) * (w - 1)));
                if (bottom && left)
                    parent.addToken(f + vp + ((hp - 1) * (w - 1)) + 1);
                if (top && right)
                    parent.addToken(f + vp + (hp * (w - 1)));
                if (bottom && right)
                    parent.addToken(f + vp + (hp * (w - 1)) + 1);

                if (parent.value == 1) {
                    if (right)
                        parent.addToken(position + w);
                    if (left)
                        parent.addToken(position - w);
                } else {
                    if (top)
                        parent.addToken(position - 1);
                    if (bottom)
                        parent.addToken(position + 1);
                }
            }
        }
    }
}
