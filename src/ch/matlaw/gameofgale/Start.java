package ch.matlaw.gameofgale;

public class Start {
    public static void main(String[] args) {
        Board board = Board.createBoard(7);

        board.setValue(0, 1);
        board.setValue(1, 1);
        board.setValue(3, 1);

        board.setValue(6, 1);
        board.setValue(7, 1);
        board.setValue(8, 1);

        long start = System.nanoTime();
        System.out.println(WinChecker.checkForWin(board));
        long end = System.nanoTime();
        System.out.println("this check needed " + (end - start) + "ns");
    }
}
