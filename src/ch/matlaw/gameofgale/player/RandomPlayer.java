package ch.matlaw.gameofgale.player;

import ch.matlaw.gameofgale.Board;
import ch.matlaw.gameofgale.Game;
import ch.matlaw.gameofgale.GuiController;

import java.util.Random;

public class RandomPlayer extends Player {
    private Random randomGenerator;
    private int bottomOffset = 1;
    private int topOffset;

    public RandomPlayer() {
        randomGenerator = new Random();
        topOffset = Game.getInstance().getCurrentBoard().calculateAmount();
    }

    @Override
    public void notifyTurn() {
        Board currentBoard = Game.getInstance().getCurrentBoard();
        boolean emptyValueFound = false;

        do {
            int value = randomGenerator.nextInt(topOffset - bottomOffset) + bottomOffset;

            if (currentBoard.getValue(value - 1) == 0) {
                currentBoard.setValue(value - 1, this.getValue());
                GuiController.getInstance().getField(value).mark(this.getValue());
                emptyValueFound = true;
            }
        } while (!emptyValueFound);

        this.turnOver = true;
    }
}
