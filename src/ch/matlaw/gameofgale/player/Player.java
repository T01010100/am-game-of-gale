package ch.matlaw.gameofgale.player;

public abstract class Player extends Thread {
    private int value;
    protected boolean turnOver;

    public abstract void notifyTurn();

    public boolean isTurnOver() {
        return turnOver;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public void resetTurnOver() {
        this.turnOver = false;
    }
}
